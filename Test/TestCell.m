//
//  TestCell.m
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import "TestCell.h"

@implementation TestCell

+(NSString*)reuseIdentifier{
    return NSStringFromClass([self class]);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [UILabel new];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor whiteColor];
        _didAnimate = NO;
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.titleLabel.frame = self.contentView.bounds;
}

-(void)prepareForReuse{
    self.titleLabel.text = nil;
    _didAnimate = NO;
    [super prepareForReuse];
}
/*
-(void)setAnimationDelay:(NSTimeInterval)animationDelay{
    _animationDelay = animationDelay;
}*/

-(void)beginAnimations:(NSNotification*)note{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"animateCells" object:nil];
    [self animateWithDelay:self.animationDelay];
    
}

-(void)animateWithDelay:(NSTimeInterval)delay{
    
    UIView *snap = [self.contentView snapshotViewAfterScreenUpdates:NO];
    
    snap.frame = [self.window convertRect:self.contentView.frame fromCoordinateSpace:self.contentView];
    
    snap.transform = CGAffineTransformMakeTranslation(0, 50);
    
    snap.alpha = 0;
    
    self.contentView.hidden = YES;
    
    __weak typeof(self) weakSelf = self;
    
    [self.window addSubview:snap];
    
    [UIView animateWithDuration:.5f
                          delay:delay
         usingSpringWithDamping:1
          initialSpringVelocity:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         snap.transform = CGAffineTransformIdentity;
                         snap.alpha = 1;
        
    } completion:^(BOOL finished) {
        
        weakSelf.contentView.hidden = NO;
        [snap removeFromSuperview];
        
    }];
    
}

@end
