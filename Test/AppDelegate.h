//
//  AppDelegate.h
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

