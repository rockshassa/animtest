//
//  TestCell.h
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestCell : UICollectionViewCell

+(NSString*)reuseIdentifier;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) NSTimeInterval animationDelay;
@property (nonatomic, assign, readonly) BOOL didAnimate;
-(void)beginAnimations:(NSNotification*)note;
@end
