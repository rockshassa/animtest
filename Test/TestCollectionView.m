//
//  TestCollectionView.m
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import "TestCollectionView.h"
#import "TestCell.h"

@implementation TestCollectionView

-(void)reloadData{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super reloadData];
}

@end
