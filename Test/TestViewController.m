//
//  TestViewController.m
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import "TestViewController.h"
#import "TestCollectionView.h"
#import "TestCell.h"

@interface TestViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) TestCollectionView *collectionView;

@property (nonatomic, strong) NSArray<NSString*> *data;

@property (nonatomic, assign) BOOL firstView;

@property (nonatomic, assign) BOOL didAnimate;

@end

@implementation TestViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    _firstView = YES;
    _didAnimate = NO;
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.75];
    
    NSMutableArray *array = [@[] mutableCopy];
    for (int i = 0; i < 10; i++) {
        [array addObject:@"InstaFaceTube"];
    }
    self.data = array;

    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.itemSize = CGSizeMake(180, 100);
    
    _collectionView = [[TestCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
    _collectionView.alpha = 0;
    
    [_collectionView registerClass:[TestCell class] forCellWithReuseIdentifier:[TestCell reuseIdentifier]];
    
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:_collectionView];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.collectionView.frame = CGRectMake(0, 0, 400, 400);
    self.collectionView.center = self.view.center;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"%s",__PRETTY_FUNCTION__);
    if (_firstView) {
        _firstView = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"animateCells" object:nil];
        self.collectionView.alpha = 1;
        _didAnimate = YES;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.data.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TestCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[TestCell reuseIdentifier] forIndexPath:indexPath];
    
    cell.titleLabel.text = self.data[indexPath.row];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.didAnimate == NO) {
    
        NSTimeInterval delayMultiplier = .25f;
        
        TestCell *c = (TestCell*)cell;
        c.animationDelay = delayMultiplier*indexPath.row;
        
        [[NSNotificationCenter defaultCenter] addObserver:c selector:@selector(beginAnimations:) name:@"animateCells" object:nil];
    }
}

@end
