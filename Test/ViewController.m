//
//  ViewController.m
//  Test
//
//  Created by Nicholas Galasso on 10/16/15.
//  Copyright © 2015 Nicholas Galasso. All rights reserved.
//

#import "ViewController.h"
#import "TestViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton *button;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    _button.backgroundColor = [UIColor blueColor];
    [self.button setTitle:@"Do Stuff" forState:UIControlStateNormal];
    [self.view addSubview:self.button];
    
    [self.button addTarget:self
                    action:@selector(tappedButton:)
          forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)tappedButton:(id)sender{
    
    TestViewController *vc = [[TestViewController alloc] init];
    vc.modalPresentationStyle = UIModalPresentationCustom;
    
    [self presentViewController:vc animated:YES completion:^{
        
    }];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.button.frame = CGRectInset(self.view.bounds, 200, 200);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
